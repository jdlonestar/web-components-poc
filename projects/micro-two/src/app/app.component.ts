import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-micro-two',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'micro-two';
  navBarMsg = '';

  ngOnInit() {
    const navbar = document.querySelector('app-nav');
    navbar.addEventListener('navEvent', (event: any) => {
      this.navBarMsg = event.detail;
    });
  }
  passDataToNavbar() {
    const nav = document.querySelector('app-nav');
    nav['name'] = 'Micro App2';
  }
}
