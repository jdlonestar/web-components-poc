const fs = require('fs-extra');
const concat = require('concat');

console.log(new Date().toDateString())

// const bundleName = 'app-nav.bundle.' + new Date().getTime() + '.js';
let fileName = 'app-nav.js';
// fileName = bundleName;

(async function build() {
  // const bundleFiles = [];
  // fs.readdir('./dist/app-nav/', (err, files) => {
  //   files.forEach(file => {
  //     if(file.indexOf('runtime.') === 0 || file.indexOf('polyfills.') === 0 || file.indexOf('main.') === 0) {
  //       bundleFiles.push(file)
  //       console.log(file);
  //     }
  //   });
  // });
  const files = [
    './dist/app-nav/runtime.js',
    './dist/app-nav/polyfills.js',
    // './dist/micro-one/scripts.js',
    './dist/app-nav/main.js'
  ];

  await fs.ensureDir('src/elements');
  await concat(files, 'src/elements/'+ fileName);
//   await fs.copyFile(
//     './dist/micro-onestyles.css',
//     'elements/styles.css'
//   );

  // https://github.com/angular/angular/issues/23732
  fs.readFile('src/elements/' + fileName, 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    var result = data.replace(/webpackJsonp/g, 'webpackJsonAppNav');
    fs.writeFile('src/elements/' + fileName, result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });


  // // to update the app configuration
  // fs.readJson('./projects/app-nav/app-nav.config.json', (err, appNavConfig) => {
  //   if (err) console.error(err)

  //   appNavConfig.bundleName = fileName;

  //   // fs.outputFile('./dist/app-nav/app-nav.config.json', appNavConfig, err => {
  //   //   console.log(err) // => null
  //   // })
  //   fs.writeJson('./dist/app-nav/app-nav.config.json', appNavConfig, err => {
  //     if (err) return console.error(err)

  //     // console.log('success!')
  //   })
  // });

})();