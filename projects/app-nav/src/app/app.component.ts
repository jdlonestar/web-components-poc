import { Component, Input, EventEmitter, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app-nav';
  @Input() public name: String = '';
  @Output() public navEvent: EventEmitter<any> =  new EventEmitter();

  passData() {
    this.navEvent.emit('Message from Nav');
  }
}
