import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';

import { MicroOneRoutingModule } from './app-routing.module';

import { Page1RouteComponent } from './page1/page1.component';
import { Page2RouteComponent } from './page2/page2-route.component';

@NgModule({
  declarations: [
    AppComponent,
    Page1RouteComponent,
    Page2RouteComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    MicroOneRoutingModule,
    RouterModule
  ],
  providers: [],
  entryComponents: [AppComponent]
})
export class MicroOneAppModule {

  constructor(injector: Injector) {
    const el = createCustomElement(AppComponent, { injector });
    if (!customElements.get('micro-one')) {
      customElements.define('micro-one', el);
    }
  }

  ngDoBootstrap() {}

}
