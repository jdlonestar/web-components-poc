import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-micro-one',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'micro-one';
  constructor(private router: Router) {

  }

  ngOnInit() {
    this.router.initialNavigation();
  }

  navigateToPage1() {
    this.router.navigate([{outlets: {'micro-one': 'page1'}}]);
  }
  navigateToPage2() {
    this.router.navigate([{outlets: {'micro-one': 'page2'}}]);
  }
  passDataToNavbar() {
    const nav = document.querySelector('app-nav');
    nav['name'] = 'Micro App1';
  }
}
