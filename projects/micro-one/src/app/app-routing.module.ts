import { Page1RouteComponent } from './page1/page1.component';
import { NgModule } from '@angular/core';
import { Page2RouteComponent } from './page2/page2-route.component';

import { RouterTestingModule } from '@angular/router/testing'

@NgModule({
  imports: [
    RouterTestingModule.withRoutes([
      { path: 'page1', component: Page1RouteComponent, outlet: 'micro-one' },
      { path: '**', redirectTo: 'page1', pathMatch: 'full'},
      { path: 'page2', component: Page2RouteComponent, outlet: 'micro-one' },
    ])
  ],
  exports: [RouterTestingModule],
})
export class MicroOneRoutingModule {

}
